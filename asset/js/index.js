// create array
// use an array literal - array literal

/*const array1 = ['eat' ,'sleep'];
console.log(array1);

const array2 = new Array('pray' , 'play');
console.log(array2);

const myList= [];

const numArray = [2,3,4,5];


const stringArray = ['eat', 'work', 'pray', 'play']

const newData = ['work', 1 , true]

const newData1 = [
	{'task1': 'exercise'},
	[1,2,3],
	function hello(){
		console.log('Hi I am array')
	}
];

console.log(newData1)


let place = ['bagiuo','tagaytay','boracay','palawan','bahay nila','batangas','ilo ilo'];

console.log(place[0]);
console.log(place)
console.log(place[place.length-1])
console.log(place.length)

let dailyActivities = ['eat','work','pray','play'];
dailyActivities.push('exercise');
console.log(dailyActivities)
dailyActivities.unshift('sleep')
console.log(dailyActivities)


place[0] = 'san mateo'
place[place.length-1] = 'roosevelt'
console.log(place);
console.log(place[0],place[place.length-1])

let array= [];
console.log(array[0])
array[0] = 'Cloud Strife';
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);
array[array.length-1] = 'Aerith Gainsborough'
console.log(array)
array[array.length] = 'Vincent valentine'
console.log(array);


let array3 = ['Juan', 'Pedro', 'Jose', 'Andres'];

array3[array.length] = 'Francisco';
console.log(array3);

array3.push('Andres');
console.log(array3)

array3.unshift('Simon')
console.log(array3)

array3.pop();
console.log(array3);

let removedItem = array3.pop();
console.log(array3);
console.log (removedItem)

console.log(array3);
array3.pop();
console.log(array3);
array3.unshift('George');
console.log(array3);
array3.push('Michael')
console.log(array3)

let numArray2 = [3,2,1,6,7,9];
numArray2.sort();
console.log(numArray2)

let numArray3 = [32, 400, 450, 2, 9, 5, 50, 90];
numArray2.sort((a,b)=> a-b);
console.log(numArray3)

let beatles = ['George','john','paul','ringo']
let lakersPlayer = [' Lebron', 'Davis','Westbrook', 'Kobe', 'Shaq']

lakersPlayer.splice(0,0,'Caruso');
console.log(lakersPlayer)
lakersPlayer.splice(0,1);
console.log(lakersPlayer);
lakersPlayer.splice(0,3);
console.log(lakersPlayer);
lakersPlayer.splice(1,1);
console.log(lakersPlayer)

let computerBrands = ['IBM', 'HP', 'Apple', 'MSI']
console.log(computerBrands)
computerBrands.splice(2,2,'Compaq', 'toshiba','acer')
console.log(computerBrands)
let newBrands = computerBrands.slice(1,3);
console.log(computerBrands);
console.log(newBrands)*/

// let fonts = ['Times New roman', 'comic san ms','impact','monotype corsiva', 'arial', 'arial black']

// console.log(fonts)

// let newFontSet = fonts.slice(1,4)
// console.log(newFontSet)
// newFontSet =fonts.slice(2);
// console.log(newFontSet)
// let newFotSet2 = fonts.slice(2);
// console.log(newFotSet2)


// let videoGame = ['PS4', 'PS5','Switch','Xbox','Xbox1']

// let microsoft = videoGame.slice(4);
// let nintendo = videoGame.slice(2,4);
// console.log(microsoft);
// console.log(nintendo);

// let setence = ['I','like','JavaScript','.','It','is','fun','.']
// let sentenceString = setence.toString();
// console.log(setence);
// console.log(sentenceString);

// let sentence2 = ['My','favorite','is','army navy']
// let sentenceString2 = sentence2.join(' ')
// console.log(sentenceString2);
// let sentencestring3 = sentence2.join('-')
// console.log(sentencestring3)


// let numArray3 = [32, 400, 450, 2, 9, 5, 50, 90];
// numArray3.sort((a,b)=> a-b);
// console.log(numArray3)


// let firstname = ['m','a','r','t','i','n'];
// let secondname = ['m','i','g','u','e','l'];

// let name1 = firstname.join('');
// let name2 = secondname.join('')
// console.log(firstname)
// console.log(name1)
// console.log(secondname)
// console.log(name2)

// //concate
// let tasksFriday = ['drink HTML', ' eat javaScript']
// let tasksSaturday = ['inhale CSS','break']
// let tasksSunday = ['Get Git', 'Be Node']

// let weekendTasks = tasksFriday.concat(tasksSaturday,tasksSunday);
// console.log(weekendTasks)

// //accessors
// 	// methods that allows us to access our array.
// 	// indexOf()
// 	// - finds the index of the given elements/ item when it is first func from the left


// let batch131 = [
// 'Paolo',
// 'Jamir',
// 'Jed',
// 'Ronel',
// 'Rom',
// 'Jayson'
// ];

// console.log(batch131.indexOf('Jed'))

// console.log(batch131.lastIndexOf('Jamir'))
// console.log(batch131.lastIndexOf('Jayson'))

// let carBrands = [
// 			'BMW',
// 			'Dodge',
// 			'Maserati',
// 			'Porsche',
// 			'Chevrolet',
// 			'Ferrari',
// 			'GMC',
// 			'Porsche',
// 			'Mitsubhisi',
// 			'Toyota',
// 			'Volkswagen',
// 			'BMW'
// 		];

// function indexing(array1,item){
// 	console.log(array1.indexOf(item));
// }
// function indexing2(array2,item2){
// 	console.log(array2.lastIndexOf(item2))
// }

// indexing(carBrands,'BMW')
// indexing2(carBrands,'BMW')

// // iterator methods
// // these methods iterate over the items in an array much like array
// // however , with our iterator methods there also that allows to not only iterate over items but also additional instruction

// let avengers = [ ' Hulk','black widow','hawkeye','spider-man','iron man', 'captain america'];

// // forEach()
// // similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration
// // anonymously function within forEach will be receive each and every item in array

// avengers.forEach(function(avenger){
// 	console.log(avengers);

// });
// console.log(avengers)

// let marvelHeroes = [
// 		'Moon Knight',
// 		'Jessica Jones',
// 		'Deadpool',
// 		'Cyclops'];

// marvelHeroes.forEach(function(hero){
// 	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
// 		avengers.push(hero);
// 	}
// });

// console.log(avengers)

// let number = [25, 50, 30, 10,5];

// let mappedNumbers =number.map(function(number){
// 	console.log(number);
// 	return number*5
// })
// console.log(mappedNumbers)

// //every()
// //iterates over all the items and checks if all the elements passes a given condition

// let allMemberAge = [25,30,15,20,26];

// let checkAllAdult = allMemberAge.every(function(age){
// 	console.log(age);
// 	return age >= 18;
// })

// console.log(checkAllAdult)

// // some()
// // iterate over all the items check if even at least one of items in the array passes the condition. same as every() it will return boolean

// let examScores = [75,80,74,71];
// let checkForPassing = examScores.some(function(score){
// 	console.log(score);
// 	return score >= 80;
// })
// console.log(checkForPassing)

// // filter() - creates a new array that contains element which passed a given condition

// let numbersArr2 = [500,12,120,60,6,30];

// let divisibleBy5 = numbersArr2.filter(function(number){
// 	return number% 5 === 0;
// })
// console.log(divisibleBy5)

// //find() iterate over all items in our array 

// let registerUsernames = ['pedro101','mikeytheking2000','superPhoenix','sheWhocode'];

// let foundUser = registerUsernames.find(function(username){
// 	console.log(username);
// 	return username ==='mikeyTheKing2000'
// })

// console.log(foundUser)

// // .include() - returns a boolean true if it finds a matching item in athe array
// // case-sensitiy

// let registeredEmail = [
// 'johnnyPhoenix1991@gmail.com',
// 'michealKing@gmail.com',
// 'pedro_himself@yahoo.com',
// 'sheJonesSmith@gmail.com'];

// let doesEmailExist = registeredEmail.includes('michealKing@gmail.com')
// console.log(doesEmailExist)

let registerUsernames = ['pedro101','mikeytheking2000','superPhoenix','sheWhocode'];

let registeredEmail = [
'johnnyPhoenix1991@gmail.com',
'michealKing@gmail.com',
'pedro_himself@yahoo.com',
'sheJonesSmith@gmail.com'];

function findingUser(username){
	let x = registerUsernames.find(function(username){
		return username
	})
	if (username != x){
			alert(`${username}, your username is available`)
		}
		else{
			alert('Your username is taken.')
		}
}

findingUser('carlos123')

function findingEmail(email){
	let y = registeredEmail.find(function(email){
		return email
	})
	if (email != y){
			alert(`${email},Your email is available.`)
		}
		else{
			alert('Your email is taken.')
		}
}
findingEmail('carlosanjose35@gmail.com')